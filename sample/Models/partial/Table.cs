﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace sample.Models
{
    [MetadataType(typeof(TableMetadata))]
    public partial class Table
    {
    }


    public partial class TableMetadata {
        public int Id { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(10)]
        public string name { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("Email")]
        [StringLength(50)]
        [EmailAddress]
        public string email { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("密碼")]
        [StringLength(10)]
        public string password { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("出生年月日")]
        public System.DateTime birthday { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("性別")]
        public bool gender { get; set; }
    }
}