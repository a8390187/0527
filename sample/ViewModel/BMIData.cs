﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace sample.ViewModel
{
    public class BMIData
    {
        [Display(Name="Height")]
        [Required(ErrorMessage ="required field")]
        [Range(50,200,ErrorMessage= "請輸入50到200的數值")]
        public float Weight { get; set; }
        [Display(Name = "Weight")]
        [Required(ErrorMessage = "required field")]
        [Range(30, 150, ErrorMessage = "請輸入30到150的數值")]
        public float Height { get; set; }
        public float BMI { get; set; }
        public string level { get; set; }
    }
}