﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using sample.ViewModel;

namespace sample.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (ModelState.IsValid)
            {
                if (data.Height < 50 || data.Height > 200)
                    ViewBag.herror = "請輸入50到200的數值";
                if (data.Weight < 30 || data.Weight > 150)
                    ViewBag.werror = "請輸入30到150的數值";
                float m_hh = data.Height / 100;
                float bmi = data.Weight / (m_hh * m_hh);

                string level = "";
                if (bmi < 18.5)
                {
                    level = "太瘦";
                }
                else if (bmi > 18.5 && bmi < 24)
                {
                    level = "適中";
                }
                else if (bmi > 35)
                {
                    level = "太胖";
                }

                data.BMI = bmi;
                data.level = level;
            }
                return View(data);
            
        }
    }
}